import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

// containers
import { StudentDashboardComponent } from './Container/student-dashboard-component';
import { StudentViewerComponent } from './container/student-viewer.component';
import { StudentCreateComponent } from './container/student-create.component';


//// components
import { StudentDetailComponent } from './Components/student-detail/student-detail.component';
import { StudentFormComponent } from './components/student-form/student-form.component';

// service
import { StudentService } from './student.service';

const routes: Routes = [
  {
    path: 'students',
    children: [
      { path: '', component: StudentDashboardComponent },
      { path: 'create', pathMatch: "full", component: StudentCreateComponent},
      { path: ':id', component: StudentViewerComponent },

    ]
  }
];

@NgModule({
  declarations: [
    StudentDashboardComponent,
    StudentViewerComponent,
    StudentDetailComponent,
    StudentFormComponent,
    StudentCreateComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    StudentService
  ]
})
export class StudentDashboardModule { }

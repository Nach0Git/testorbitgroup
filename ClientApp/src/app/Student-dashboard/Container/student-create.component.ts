import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { switchMap } from 'rxjs/operators';


import { StudentService } from '../student.service';

import { Student } from '../../Interfaces/StudentInterface';

@Component({
  selector: 'Student-create',
  template: `
    <div>
      <Student-form
        [detail]="student"
        (update)="onUpdatestudent($event)">
      </Student-form>
    </div>
  `
})
export class StudentCreateComponent implements OnInit {
  student: Student;
  constructor(   
    private router: Router,
    private studentService: StudentService
  ) {}
  ngOnInit() {

  }
  onUpdatestudent(event: Student) {
    event.id=0;
    this.studentService
      .createStudent(event)
      .subscribe((data: any) => {
        this.router.navigate(["/students"])
      }); 
  }
}
import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';

import { StudentService } from '../student.service';

import { Student } from '../../Interfaces/StudentInterface'


@Component({
  selector: 'student-dashboard',
  template: `
      <student-detail
        *ngFor="let student of students"
        [detail]="student"
        (edit)="handleEdit($event)"
        (remove)="handleRemove($event)">
      </student-detail>
  `
})
export class StudentDashboardComponent implements OnInit {
  public students: Student[];
  constructor(private studentService: StudentService, private router: Router) {}
  ngOnInit() {
     this.studentService
      .getStudents()
        .subscribe((result)=> {
            console.log(result);
            this.students= result;
            }
        );
  }
  handleEdit(event: Student) {
      debugger;
      this.router.navigate([`/students/${event.id}`])
  }
  handleRemove(event: Student) {
      this.studentService
        .removeStudent(event)
            .subscribe(result=> {
                this.students = this.students.filter((student: Student) => {
                return student.id !== result;
                })
            }, error=> console.error(error))
      
  } 
}
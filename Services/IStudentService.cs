﻿using System.Collections.Generic;
using PruebaAngular.DAL.Models;

namespace PruebaAngular.Services
{
    public interface IStudentService
    {
        List<Student> GetAllStudents();

        Student GetStudent(int id);

        int Delete(int id);

        void UpdateStudent(Student student);

        void Create(Student student);
    }
}